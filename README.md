PYP_dmdmd_bw
======


Install and Use
-------------

Set up gsissh access to Blue Waters

Set up virtual environment:

	virtualenv $HOME/myenv
	source $HOME/myenv/bin/activate

Install radical.ensemblemd:

	git clone git@github.com:radical-cybertools/radical.ensemblemd.git
	pip install . --upgrade
(currently need to install the devel branch)

download this repository:

	git@github.com:ClementiGroup/PYP_dmdmd_bw.git


generate set of replicas from starting structure: (big file)

	cd pyp_test/inp_files
	sh cp.sh
	cd ..


Change settings in bw.rcfg:

	UNAME, walltime and pilotsize

Start simulation:

	RADICAL_ENMD_VERBOSE=info python 01_static_gromacs_lsdmap_loop.py --RPconfig bw.rcfg --Kconfig gromacslsdmap.wcfg


